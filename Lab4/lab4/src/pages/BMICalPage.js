import BMIResult from '../components/BMIResult.js'
import {useState} from "react";

function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState("");
    const [ TranslateResult, setTranslateResult ] = useState("");

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);
        if(bmi>25){
            setTranslateResult("อ้วน");
        }else if(bmi<20){
            setTranslateResult("ผอม");
        }else{
            setTranslateResult("หุ่นดี");
        }
    }

    return(
    <div align = "left">
        <div align = "center">
            <br/>
            <h1>Welcome to BMI Calculator</h1>
            <br/>

            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>

            <br/>
            <h3>ส่วนสูง (m) : <input type = "text" value={height} onChange={(e) =>{setHeight(e.target.value);}}/>
            </h3>

            <h3>น้ำหนัก (kg) : <input type = "text" value={weight} onChange={(e) => {setWeight(e.target.value);}}/></h3>
            <br />
            <button onClick={(e) => {calculateBMI()}}>Calculate</button>
            <br/>
            <br/>
            <br/>

            { bmiResult != 0 && 
                <div>
                    <h3>ผลการคำนวณ</h3>
                    <BMIResult
                        name ={ name }
                        BMI = { bmiResult }
                        result = {TranslateResult}
                    />
                </div>
            }
                    
        </div>
    </div>
    );
}

export default BMICalPage;