import {Link} from "react-router-dom"
function Header() {

    return (
        <div align = "left">
            <p>Welcome to BMI calculator : &nbsp;
                <Link to="/">เครื่องคิดเลข</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">ผู้พัฒนา</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/contact">ติดต่อ</Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="/LuckyNumber">สุ่มเลข</Link>
            </p>
        </div>
    );
}

export default Header;